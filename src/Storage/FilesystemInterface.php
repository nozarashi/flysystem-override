<?php

declare(strict_types=1);

namespace App\Storage;

use League\Flysystem\FilesystemAdapter;
use League\Flysystem\FilesystemOperator;

interface FilesystemInterface extends FilesystemOperator
{
    public function getAdapter(): FilesystemAdapter;

    public function getAbsolutePath(string $path): ?string;
}