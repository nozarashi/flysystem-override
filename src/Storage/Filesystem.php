<?php

declare(strict_types=1);

namespace App\Storage;

use League\Flysystem\FilesystemAdapter;
use League\Flysystem\PathNormalizer;

class Filesystem extends \League\Flysystem\Filesystem implements FilesystemInterface
{
    private FilesystemAdapter $adapter;

    public function __construct(FilesystemAdapter $adapter, array $config = [], PathNormalizer $pathNormalizer = null)
    {
        parent::__construct($adapter, $config, $pathNormalizer);
        $this->adapter = $adapter;
    }

    public function getAdapter(): FilesystemAdapter
    {
        return $this->adapter;
    }

    public function getAbsolutePath(string $path): ?string
    {
        return $this->adapter instanceof LocalAdapter ? $this->adapter->applyPrefix($path) : null;
    }
}