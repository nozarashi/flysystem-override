<?php

declare(strict_types=1);

namespace App\Storage;

use League\Flysystem\Local\LocalFilesystemAdapter;
use League\Flysystem\PathPrefixer;
use League\Flysystem\UnixVisibility\VisibilityConverter;
use League\MimeTypeDetection\MimeTypeDetector;

class LocalAdapter extends LocalFilesystemAdapter
{
    private string $location;

    private PathPrefixer $prefixer;

    public function __construct(string $location, VisibilityConverter $visibility = null, int $writeFlags = LOCK_EX, int $linkHandling = self::DISALLOW_LINKS, MimeTypeDetector $mimeTypeDetector = null)
    {
        parent::__construct($location, $visibility, $writeFlags, $linkHandling, $mimeTypeDetector);

        $this->location = $location;
        $this->prefixer = new PathPrefixer($location, DIRECTORY_SEPARATOR);
    }

    public function getLocation(): string
    {
        return $this->location;
    }

    public function applyPrefix(string $path): string
    {
        return $this->prefixer->prefixPath($path);
    }
}