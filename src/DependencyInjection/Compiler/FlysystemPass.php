<?php

declare(strict_types=1);

namespace App\DependencyInjection\Compiler;

use App\Storage\FilesystemInterface;
use App\Storage\LocalAdapter;
use League\Flysystem\Filesystem;
use League\Flysystem\Local\LocalFilesystemAdapter;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class FlysystemPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {

        $taggedServiceIds = $container->findTaggedServiceIds('flysystem.storage');

        foreach ($taggedServiceIds as $id => $tags) {
            $definition = $container->findDefinition($id);
            if ($definition->getClass() === Filesystem::class) {
                $definition->setClass(\App\Storage\Filesystem::class);
                $container->setDefinition($id, $definition);

                $adapterId = sprintf('flysystem.adapter.%s', $id);
                $adapterDefinition = $container->findDefinition($adapterId);
                if ($adapterDefinition->getClass() === LocalFilesystemAdapter::class) {
                    $adapterDefinition->setClass(LocalAdapter::class);
                    $container->setDefinition($adapterId, $adapterDefinition);
                }
                $container->registerAliasForArgument($id, FilesystemInterface::class, $id)->setPublic(false);
            }
        }
    }
}