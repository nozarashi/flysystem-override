<?php

declare(strict_types=1);

namespace App\Controller\Document;

use App\Entity\Document;
use App\Form\UploadDocumentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UploadController extends AbstractController
{
    #[Route('/doc/upload', name: 'doc_upload')]
    public function __invoke(Request $request, EntityManagerInterface $em): Response
    {
        $document = new Document();
        $form = $this->createForm(UploadDocumentType::class, $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($document);
            $em->flush();
            $this->addFlash('success', 'Document uploaded');
        }

        return $this->renderForm('document/upload.html.twig', [
            'form' => $form
        ]);
    }
}