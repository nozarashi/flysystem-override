<?php

declare(strict_types=1);

namespace App\Controller\Document;

use App\Entity\Document;
use App\Storage\FilesystemInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DetailsController extends AbstractController
{
    public function __construct(private FilesystemInterface $defaultStorage)
    {
    }

    #[Route('/doc/{id}', name: 'doc_detail')]
    public function __invoke(Document $document, ): Response
    {
        $absolutePath = $this->defaultStorage->getAbsolutePath($document->getPath());

        return $this->render('document/details.html.twig', [
            'document' => $document,
            'absolutePath' => $absolutePath,
        ]);
    }
}